package mergegate_test

import (
	"testing"

	"bitbucket.org/johnweldon/mergegate"
	"bitbucket.org/johnweldon/mergegate/types"
)

func TestCanGetPullRequests(t *testing.T) {
	repo := canGetRepository(t, "", "", "https://bitbucket.org/johnweldon/mergegate")
	tests := []struct {
		fn     getPullRequestFn
		expect int
	}{
		{fn: repo.PullRequests, expect: 4},
		{fn: repo.OpenPullRequests, expect: 1},
		{fn: repo.MergedPullRequests, expect: 2},
		{fn: repo.DeclinedPullRequests, expect: 1},
	}

	for ix, test := range tests {
		t.Logf("test case %d, expecting %d results", ix, test.expect)
		prs := canGetPullRequests(t, test.fn)
		if len(prs) != test.expect {
			t.Errorf("unexpected number of pull requests; expected %d, got %d:\n %#v\n", test.expect, len(prs), prs)
		}
	}
}

func TestCanGetPullRequest(t *testing.T) {
	repo := canGetRepository(t, "", "", "https://bitbucket.org/johnweldon/mergegate")
	tests := []struct {
		id          string
		expectTitle string
		expectState string
		expectError string
	}{
		{id: "5", expectError: "Resource not found"},
		{id: "1", expectTitle: "Add MIT License", expectState: "MERGED"},
		{id: "2", expectTitle: "ignores", expectState: "MERGED"},
		{id: "3", expectTitle: "Super Crazy PR", expectState: "DECLINED"},
		{id: "4", expectTitle: "Deferred PR", expectState: "OPEN"},
	}
	for ix, test := range tests {
		t.Logf("test case %d, testing pull request %q", ix, test.id)
		pr, err := repo.PullRequest(test.id)
		switch {
		case len(test.expectError) > 0:
			switch err {
			case nil:
				t.Errorf("expected error %q, got no error", test.expectError)
				t.Logf("PR %+v", pr)
			default:
				if test.expectError != err.Error() {
					t.Errorf("expected error %q, got %q", test.expectError, err)
				}
			}
		default:
			if pr.Title != test.expectTitle {
				t.Errorf("expected Title %q, got %q", test.expectTitle, pr.Title)
			}
			if pr.State != test.expectState {
				t.Errorf("expected State %q, got %q", test.expectState, pr.State)
			}
		}

	}
}

func canGetRepository(t *testing.T, username, password, repoURL string) mergegate.Repository {
	repo, err := mergegate.OpenRepository(repoURL, username, password)
	mergegate.Cast(repo).StubContentRetriever(testRetriever)
	if err != nil {
		t.Fatalf("error opening repository %q: %q", repoURL, err)
	}
	if repo == nil {
		t.Fatalf("error opening repository %q: repo is nil", repoURL)
	}
	return repo
}

type getPullRequestFn func() ([]types.PullRequest, error)

func canGetPullRequests(t *testing.T, fn getPullRequestFn) []types.PullRequest {
	pullRequests, err := fn()
	if err != nil {
		t.Fatalf("error retrieving pull requests: %q", err)
	}
	return pullRequests
}
