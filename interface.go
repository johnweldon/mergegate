package mergegate

import "bitbucket.org/johnweldon/mergegate/types"

// Repository represents the master repository that the pull requests
// are targeting.
type Repository interface {
	PullRequests() ([]types.PullRequest, error)
	OpenPullRequests() ([]types.PullRequest, error)
	MergedPullRequests() ([]types.PullRequest, error)
	DeclinedPullRequests() ([]types.PullRequest, error)
	PullRequest(name string) (types.PullRequest, error)
}

// WebContentRetriever allows us to intercept, or mock out actual
// http requests.
type WebContentRetriever interface {
	Fetch(url string) ([]byte, error)
}
