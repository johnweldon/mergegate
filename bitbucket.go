package mergegate

import (
	"encoding/json"
	"fmt"
	"net/url"
	"strings"

	"bitbucket.org/johnweldon/mergegate/types"
)

type bitbucketRepository struct {
	username  string
	password  string
	url       *url.URL
	owner     string
	repoSlug  string
	retriever WebContentRetriever
}

func (r *bitbucketRepository) PullRequests() ([]types.PullRequest, error) {
	return r.pullRequests("OPEN,MERGED,DECLINED")
}

func (r *bitbucketRepository) OpenPullRequests() ([]types.PullRequest, error) {
	return r.pullRequests("OPEN")
}

func (r *bitbucketRepository) MergedPullRequests() ([]types.PullRequest, error) {
	return r.pullRequests("MERGED")
}

func (r *bitbucketRepository) DeclinedPullRequests() ([]types.PullRequest, error) {
	return r.pullRequests("DECLINED")
}

func (r *bitbucketRepository) PullRequest(name string) (types.PullRequest, error) {
	var response = types.PullRequest{}
	body, err := r.fetch(r.pullRequestURL(name))
	if err != nil {
		return response, err
	}

	err = json.Unmarshal(body, &response)
	return response, err
}

func (r *bitbucketRepository) pullRequests(state string) ([]types.PullRequest, error) {
	body, err := r.fetch(r.pullRequestsURL(state))
	if err != nil {
		return nil, err
	}

	var er types.ErrorResponse
	err = json.Unmarshal(body, &er)
	if err != nil {
		return nil, err
	}
	if len(er.Error.Message) > 0 {
		return nil, fmt.Errorf(er.Error.Message)
	}

	var response types.PullRequestsResponse
	err = json.Unmarshal(body, &response)
	if err != nil {
		return nil, err
	}
	return response.Values, nil
}

func (r *bitbucketRepository) pullRequestURL(name string) string {
	return r.baseURL() + r.repositoryPath() + "/pullrequests/" + name
}

func (r *bitbucketRepository) pullRequestsURL(state string) string {
	return r.baseURL() + r.repositoryPath() + "/pullrequests?state=" + state
}

func (r *bitbucketRepository) repositoryPath() string {
	return "repositories/" + r.owner + "/" + r.repoSlug
}

func (r *bitbucketRepository) baseURL() string {
	var auth string
	if len(r.password) > 0 {
		auth = r.username + ":" + r.password + "@"
	}
	return "https://" + auth + "api.bitbucket.org/2.0/"
}

func (r *bitbucketRepository) fetch(url string) ([]byte, error) {
	body, err := r.retriever.Fetch(url)
	if err != nil {
		return nil, err
	}

	var er types.ErrorResponse
	err = json.Unmarshal(body, &er)
	if err != nil {
		return nil, err
	}
	if len(er.Error.Message) > 0 {
		return nil, fmt.Errorf(er.Error.Message)
	}

	return body, nil
}

func openBitbucketRepository(repoURL, username, password string) (Repository, error) {
	parsed, err := url.Parse(repoURL)
	if err != nil {
		return nil, err
	}
	parts := strings.Split(parsed.Path[1:], "/")
	if len(parts) != 2 {
		return nil, fmt.Errorf("path not correct for bitbucket: %#v", parts)
	}

	return &bitbucketRepository{
		username:  username,
		password:  password,
		url:       parsed,
		owner:     parts[0],
		repoSlug:  parts[1],
		retriever: &httpRetriever{},
	}, nil
}
