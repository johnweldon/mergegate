#Merge Gate

Merge Gate will be a tool for gating merges in BitBucket, and possibly 
in other environments later too.

## Areas to be developed

### Polling and Updating Pull Requests

 1. Retrieving list of pending Pull Requests
 1. Retrieving repository and merge information about a specific Pull 
    Request
 1. Detecting Pull Requests with a 'merge proposal' in the comments, or
    with 'approval'
 1. Updating Pull Request with approval or rejection and rationale

### Syncing, Merging, and testing pull requests

 1. Local merging of Pull Request and running user defined tests to
    verify readiness of Pull Request
 1. Optionally performing and pushing real merge on success

### Configuration Management

 1. Storing credentials, repository information, build information, etc.
 1. Potentially storing 'business rules' type configuration. e.g. minimum
    number of approvers, minimum test coverage, etc.

