package mergegate

// OpenRepository takes the credentials and url for a repository and
// returns the structure used to get pull request and other information.
func OpenRepository(url, username, password string) (Repository, error) {
	return openBitbucketRepository(url, username, password)
}
