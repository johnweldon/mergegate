package types

import "time"

// RepositoryInfo describes basic information about a repository.
type RepositoryInfo struct {
	FullName   string    `json:"full_name"`
	Name       string    `json:"name"`
	Links      Links     `json:"links"`
	Owner      User      `json:"owner,omitempty"`
	SCM        string    `json:"scm,omitempty"`
	ForkPolicy string    `json:"fork_policy,omitempty"`
	Language   string    `json:"language,omitempty"`
	Created    time.Time `json:"created_on,omitempty"`
	Updated    time.Time `json:"updated_on,omitempty"`
	HasIssues  bool      `json:"has_issues,omitempty"`
	HasWiki    bool      `json:"has_wiki,omitempty"`
	IsPrivate  bool      `json:"is_private,omitempty"`
	UUID       string    `json:"uuid,omitempty"`
	Size       int       `json:"size,omitempty"`
}

// PullRequest describes the information about a pull request.
type PullRequest struct {
	ID                int           `json:"id"`
	Author            User          `json:"author"`
	Title             string        `json:"title"`
	Description       string        `json:"description"`
	Destination       Reference     `json:"destination"`
	Source            Reference     `json:"source"`
	MergeCommit       Commit        `json:"merge_commit"`
	State             string        `json:"state"`
	Reason            string        `json:"reason"`
	ClosedBy          User          `json:"closed_by"`
	Reviewers         []User        `json:"reviewers"`
	Participants      []Participant `json:"participants"`
	Created           time.Time     `json:"created_on"`
	Updated           time.Time     `json:"updated_on"`
	Links             Links         `json:"links"`
	CloseSourceBranch bool          `json:"close_source_branch"`
}

type User struct {
	Username    string `json:"username"`
	DisplayName string `json:"display_name"`
	Links       Links  `json:"links"`
}

type Participant struct {
	Role     string `json:"role"`
	User     User   `json:"user"`
	Approved bool   `json:"approved"`
}

type Reference struct {
	Commit     Commit         `json:"commit"`
	Repository RepositoryInfo `json:"repository"`
	Branch     Branch         `json:"branch"`
	Links      Links          `json:"links"`
}

type Commit struct {
	Hash  string `json:"hash"`
	Links Links  `json:"links"`
}

type Branch struct {
	Name string `json:"name"`
}

type Link struct {
	Href string `json:"href"`
	Name string `json:"name"`
}

type Links struct {
	Self         Link   `json:"self,omitempty"`
	Html         Link   `json:"html,omitempty"`
	Avatar       Link   `json:"avatar,omitempty"`
	Code         Link   `json:"code,omitempty"`
	Comments     Link   `json:"comments,omitempty"`
	Activity     Link   `json:"activity,omitempty"`
	Watchers     Link   `json:"watchers,omitempty"`
	Commits      Link   `json:"commits,omitempty"`
	PullRequests Link   `json:"pullrequests,omitempty"`
	Forks        Link   `json:"forks,omitempty"`
	Approve      Link   `json:"approve,omitempty"`
	Merge        Link   `json:"merge,omitempty"`
	Decline      Link   `json:"decline,omitempty"`
	Patch        Link   `json:"patch,omitempty"`
	Diff         Link   `json:"diff,omitempty"`
	Clone        []Link `json:"clone,omitempty"`
}

type PullRequestsResponse struct {
	PageLen int           `json:"pagelen"`
	Next    string        `json:"next"`
	Page    int           `json:"page"`
	Size    int           `json:"size"`
	Values  []PullRequest `json:"values"`
}

type ErrorResponse struct {
	Error ErrorMessage `json:"error,omitempty"`
}

type ErrorMessage struct {
	Message string `json:"message"`
}
