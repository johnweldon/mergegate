package mergegate

func (b *bitbucketRepository) StubContentRetriever(r WebContentRetriever) { b.retriever = r }
func (b *bitbucketRepository) PullRequestURL(name string) string          { return b.pullRequestURL(name) }
func (b *bitbucketRepository) PullRequestsURL(state string) string        { return b.pullRequestsURL(state) }

func Cast(r Repository) *bitbucketRepository { return r.(*bitbucketRepository) }
