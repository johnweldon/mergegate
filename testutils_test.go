package mergegate_test

import (
	"fmt"
	"io/ioutil"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	SetUpSuite()
	result := m.Run()
	TearDownSuite()
	os.Exit(result)
}

func SetUpSuite() {
	populateRetriever(testRetriever)
}

func TearDownSuite() {}

var testRetriever = &stubRetriever{urls: map[string][]byte{}}

type stubRetriever struct {
	urls map[string][]byte
}

func (r *stubRetriever) Fetch(url string) ([]byte, error) {
	if content, ok := r.urls[url]; ok {
		return content, nil
	}
	return nil, fmt.Errorf("no content for url %q", url)
}

func populateRetriever(r *stubRetriever) {
	r.urls = map[string][]byte{
		"https://api.bitbucket.org/2.0/repositories/johnweldon/mergegate/pullrequests?state=OPEN,MERGED,DECLINED": getJSONContent("./json/pullrequests.json"),
		"https://api.bitbucket.org/2.0/repositories/johnweldon/mergegate/pullrequests?state=OPEN":                 getJSONContent("./json/openpullrequests.json"),
		"https://api.bitbucket.org/2.0/repositories/johnweldon/mergegate/pullrequests?state=MERGED":               getJSONContent("./json/mergedpullrequests.json"),
		"https://api.bitbucket.org/2.0/repositories/johnweldon/mergegate/pullrequests?state=DECLINED":             getJSONContent("./json/declinedpullrequests.json"),
		"https://api.bitbucket.org/2.0/repositories/johnweldon/mergegate/pullrequests/1":                          getJSONContent("./json/pullrequest1.json"),
		"https://api.bitbucket.org/2.0/repositories/johnweldon/mergegate/pullrequests/2":                          getJSONContent("./json/pullrequest2.json"),
		"https://api.bitbucket.org/2.0/repositories/johnweldon/mergegate/pullrequests/3":                          getJSONContent("./json/pullrequest3.json"),
		"https://api.bitbucket.org/2.0/repositories/johnweldon/mergegate/pullrequests/4":                          getJSONContent("./json/pullrequest4.json"),
		"https://api.bitbucket.org/2.0/repositories/johnweldon/mergegate/pullrequests/5":                          getJSONContent("./json/pullrequest5.json"),
		"https://api.bitbucket.org/2.0/repositories/johnweldon/mergegate/forks":                                   getJSONContent("./json/repositoryforks.json"),
		"https://api.bitbucket.org/2.0/repositories/johnweldon/mergegate":                                         getJSONContent("./json/repository.json"),
		"": []byte{},
	}
}

func getJSONContent(path string) []byte {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return []byte(`{"error": {"message": "Resource not found"}}`)
	}
	return data
}
